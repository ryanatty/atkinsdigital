---
layout: post
title:  "Art and Virtual Reality Panel"
date:   2018-10-24 15:32:14 -0300
categories: virtual reality 
permalink: /thoughts/art-and-virtual-reality-panel/
---
Seattle Startup Week is a yearly event that offers a little bit of something for everyone who is part of the local startup scene. There are speaker and panel disussions covering all sorts of idustries and topics. I was lucky to be invovled in a number of virtual reality panels this year and got to moderate a panel on the topic of art and virtual reality. My fellow panelists were Simon Manning, Scott "scobot" Bennett, Marisa Erven, and Ilen Halo, all memebrs of the Seattle VR community.

Here are the questions I asked the panelists:

## Space

Art philosopher Susanne K Langer, in her 1953 book, "Feeling and Form", say that space "is the substrate of all our experience, gradually discovered by the collaboration of our several senses." As spatially located beings, space is significant to us in ways that may not seem obvious. It is integral to the way we think, plan, and behave. We shape it, and it shapes us. 
 
_How do you use virtual space to achieve your desired effect?_

## Experience

In his 1934 book "Art as Experience," John Dewey says, "the actual work of art is what the product does with and in experience." In other words, art happens when a piece is perceived and experienced. Dewey also places an emphasis on the "consumer" or art and their active participation in the experience. 
 
_How do you use virtual reality to create experience for your consumers?_

## Tools

Barriers to entry for content creators seem to be crumbling faster than ever as new tools and devices go to market.  

_What are some of the tools you use to create your pieces of art and what are some of the challenges you continue to face as content creators?_

## Critique 
 
In the 1992 VR cult classic, "The Lawnmower Man," the idea is put forth that virtual reality may be "the most transformative medium in the history of humanity," or "the greatest form of mind control every devised." 

Artaud says that he proposes to "treat the spectators like the snakecharmer's subjects and conduct them by means of their organisms to an apprehension of the subtlest of notions." 
 
Jaron Lanier, father of VR, warns against asymmetric power dynamics between producer and consumer. Tom Furness, grandfather of VR, describes virtual reality in Promethean terms. Even Plato warned against art's ability to destabilized society if left unchecked. 
 
_What Pandora's box are we opening?_

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/DOJ2cH3rR0I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>