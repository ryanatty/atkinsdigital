---
layout: post
title:  "Microsoft MR Airlift Hackathon Vlog!"
date:   2018-06-19 15:32:14 -0300
categories: mixedreality hackathon microsoft
permalink: /thoughts/microsoft-mr-airlift-hackathon-vlog/
---
_Read more about the MR Airlift experience in my [Microsoft MR Airlift Reflections: Our HoloLens Inventory App](/ideas/microsoft-mr-airlift-reflections/) post._

<iframe width="560" height="315" src="https://www.youtube.com/embed/jf2Ht5YLCeo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/KzyG_d6221o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/_qh3AQkTdv4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Pi7kar_QA50" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/bn95kK5OPVQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/KssEyRxeElw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/cAGtwQI1G5s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/JnRG1R7jNLc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

_Read more about the MR Airlift experience in my [Microsoft MR Airlift Reflections: Our HoloLens Inventory App](/ideas/microsoft-mr-airlift-reflections/) post._