---
layout: post
title:  "The Need for Standards Across Immersive Technology"
date:   2018-02-14 15:32:14 -0300
categories: immersive content standards
permalink: /thoughts/the-need-for-standards-across-immersive-technology/
---
The current technical landscape of AR/VR is impressive for sure. The barrier to entry for new developers is so much lower than what it was even a year ago. But it’s also a fractured landscape; one that lacks the necessary standards to unify its differentiated software and hardware. This is especially true when it comes to 3D content for AR/VR applications.

When Tim Berners-Lee created the Internet, he also created HTML. Based heavily on Standard Generalized Mark-up Language, HTML was a method of imparting structure onto a text document so that a browser could render it appropriately. HTML, and the later advancements in display and scripting languages, became the standards by which web content was described. These standards enabled the content to be entirely separate and independent of the browser that rendered it.

This separation of content and renderer was vital to the free expression of the early web, but this does not exist for AR/VR content. Content is inextricably linked to the application that renders it. And this is a problem.

The central argument here is that the web was a bottom up evolution with the tools and standards being developed in parallel. We have the opposite situation with the AR/VR technologies however. The tools are built, but we don't have any standards, especially standards relating to content.

In the next five years, we’re sure to see a continued fracturing of the AR/VR hardware and software ecosystem. At Odd Atom, we realize the lack of standards will hold things back, and we are working around this problem by building a content platform for AR/VR content. As a company, Odd Atom is excited for the future of AR/VR, and equally excited about providing solutions for AR/VR content.