---
layout: post
title:  "The Case for Immersive Content Portability"
date:   2018-02-14 15:32:14 -0300
categories: immersive content
permalink: /thoughts/the-case-for-immersive-content-portability/
---
Immersive content portability means that content is able to be accurately represented across the spectrum of immersive experiences. Whether VR, AR, MR, head mounted display (HMD) or mobile device, the content should be portable, just as the PDF makes documents portable across display technology.

The fact that immersive content is not currently portable is a problem for the technology as a whole.

To illustrate why portability is necessary, consider two hypothetical immersive soccer apps:

- _**VR Soccer**_ is a VR game played on a HMD.
- _**SoccAR**_ is an AR game played on a mobile device.

Given that the immersive experience between the two apps is qualitatively different (VR vs. AR), you would assume that the apps are fundamentally different. But from a content standpoint, there are quite a number of similarities. Chief among these is that each app features a soccer ball. 

The soccer ball in each app will have to look and act like a soccer ball if the app is going to make sense for the user. This is the case regardless of whether it's VR or AR, HMD or mobile.

The problem with this is that each app developer is forced to create essentially the same soccer ball. This is a duplication of effort on an element of each game that is non-differentiated. In other words, each developer is reinventing the wheel.

![Two immersive apps](/post-assets/images/1.jpg)

A better scenario is one where each developer can leverage a portable immersive “soccer ball” asset. Each app would reference this external soccer ball asset when it's needed. This external asset would be self-defining, telling the app not only how it should look, but also how it should act. Therefore there is no duplication of effort and no reinventing the wheel. The soccer ball just works.

![Two immersive apps share content](/post-assets/images/0.jpg)

The goal here is a separation of the immersive content from the application that it appears in. You don't upload 20 copies of the same video to different places on the web, you upload one copy to YouTube, and then embed it on 20 pages. The embedded video is entirely separate from the page it appears on. 

At Odd Atom, we are hard at work to make this portability a reality, which will help realize the true potential of this transformative form of computing.