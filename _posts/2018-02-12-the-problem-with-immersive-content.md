---
layout: post
title:  "The Problem with Immersive Content"
date:   2018-02-12 15:32:14 -0300
categories: immersive content
permalink: /thoughts/the-problem-with-immersive-content/
---
It would be understandable if you didn't give AR/VR content much thought. "What do you mean 'content'?" might be your first question. And that's a problem...

Now, what if you were to think about web content? You likely think images, videos, text, and audio, and you are correct. 

Chances are, all you think of is content that's accessible through Google, Facebook, Instagram, and the like, not simply individual web sites containing that content. After all, the same images, videos, text, and audio show up on Google, Facebook, YouTube, and any number of other places around the web. Not just on single web page.

The point here is that we have become accustomed to web content being accessible wherever, and whenever, we want it. Not to mention on whatever device we happen to be using at the time. 

What makes this accessibility possible is that web content is portable across the web, apps, and any device we want to use. As consumers and creators of this content, we get value from it only because it is portable to every aspect of our daily lives.

But what if the picture you just took with your phone could only be viewed with the phone's photo album app? How would you share that picture on Instagram or post it to Facebook? What if Google couldn't read and index the content on your website? How would people find your latest blog post? What if this was still the early 90's and the world wide web resembled something more akin to islands of content in a vast ocean, rather than a hyper-connected city where content is connected to people, and people to content? What good would the web be to the average user?

Well, it's the early 90's for AR/VR and we have islands of AR/VR content in a vast ocean.

We are on the precipice of a truly transformative phase in computing, but we have a number of problems to work out before that transformation can occur. At Odd Atom, we're currently hard at work on this issue of AR/VR content portability, and we're super excited for what the future will bring.