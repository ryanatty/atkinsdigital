---
layout: page
title: "Space and Time: Virtual Reality and Blockchain"
permalink: /presentations/design-reality-space-time/
---
**Topic:** Virtual Reality and Blockchain

**General Purpose:** To inform

**Specific Purpose:** To inform the audience about the essential characteristics of both virtual reality and blockcain; space and time.

**Thesis:** That a shared consensus of space and time are essental in creating a metaverse, and that the essential characteristics of virtual reality and blockchain can leveraged to create this shared consensus.

1. Introduction
    1. **Attention Getter:** 
        - Question to the audience, "when was the term _virtual reality_ first used?"
        - French playwrite Antonin Artaud wrote about _virtual realities_ in his 1938 book "The Theater and Its Double."
        - These virtual realites refered to immersive theatrical performances that aimed to put the viewer in the center and convey an impression through non-literal means.
            - _**Need quote from book**_
    2. **Reason to Listen**
        - Concepts are distinct from the modalities with which they are expressed. 
        - Virtual Reality, for instance, does not begin and end with a headset, a headset is a modal of expression of a virtual reality.
        - I believe that making concepts distinct from specific forms of expression, enables us to be more creative in our pursute of expression.
    3. **Thesis Statement**
        - Thus, my thesis, and the intention of this presentation, is to propose that the essential characteristics of virtual reality and blockchain can leveraged to create this shared consensus and that this shared consensus of space and time are essental in creating a distributed metaverse.
    4. **Credibility Statement**
        - Involved in the Seattle VR sceen for four years
        - Had a startup for two years
        - Working on docu series called Inside Out
        - Helping to organize Token Forum
    5. **Review of Main Points**
        - I want to start with a discusison about space and popose that is the essential character of virtural reality.
            - _VR = Sapce_
        - I will then move on to discuss time as being the essential characteristic of blockchain
            - _Blockchain = Time_
        - I will then combine those two concepts and show how space and time work to form identity and that decentralized record of time will be nessisary in establishing a shared reality
            - _VR +  Blockchain = Shared realites with history_
        - Finally a call to action that we must break free of thinking in the current paradime to truly take advantage of these new technologies
2. Intro to main content, mention snowcrash and that now we're all talking about the idea of the metaverse these days.
    1. Virtual reality is central to the concept of the metavers and space is central to virtual reality.
        - In his paper entitled "The Intelligent Use of Space," David Kirsh points out that..
            - _"Studies of planning have typically focused on the temporal ordering of action."_
            - I think there is a similar argument to be made of virtual reality, in that space has largely been taken for granted. Like the air we breathe, we don't notice it when it's there, but certaily when it's not there.
            - Kirsh says that of space...
                - _"[it] is not an afterthought; it is an integral part of the way we think, plan and behave, a central element in the way we shape the very world that constrains and guides our behavior."_
            - We construct spatial hierarchies to solve problems and reduce cognative load.
                - Take for example the lasua I made over the weekend, the directions say to devide into five parts in order to adaquatly serve. I don't need to weigh the portions, I don't need to know the volum of each protion, I just need to dived one whole into five pieces.
        - Not only is space used to solve problems, it is core to experience.
            - Susan Langer describes space as...
                - _"space is a substraite of experience"_<sup>2</sup>
        - VR tech's greates benifit is that it gives us amorphus space to form and make our own. 
        - **Transition:** Experience is one thing, but experince over time allows us to develope an identity
    2. Time is the essential character of blockchain
        - Imutable sequence of events over time.
        - Identity is formed when events are understood to have happened over time.
            - Memento movie as an example, snapshots of time
        - **Transition:** This is where a transition to the call to action goes.
    3. Reality is consensus
        - Lanier's definition of virtual reality
            - _A "reality" results when a mind has faith that other minds share enough of the same world to establish comminication and empathy_ <sup>4</sup>
        - Example of tree rings and rock strata
            - The difference between worlds and realites
        - Core to this concept of faith is _trust_ and this is where the decentralization of blockchain comes in.
        - As it relates to a potential metaverse's history, it's users need to trust that they are
        - Those who control the present, control the past, those who control the past control the past. <sup>5</sup>
        - Blockchain is a democratic technology because it is concensus based
3. Conclution
    1. **Review of main points**
        - Space is the essential character of virtural reality
        - Time is the essential character of blockchain
        - Reality forms when we have faith of a shared historic experience
    2. **Restate Thesis Statement**
        - This is important because it shows that theories are distinct from practical methodology
    3. **Closure**
        - Marshall McLuhan's "The medium is the message" <sup>6</sup>
            - A new environment of possiblility with VR and blockchain
            - Avoid thinking in old terms 
                - "excell spreadsheet in space"
                - "Nextflix in space"
        - Narcissus quote about looking into the water and being dulled
            - Look for the message, ignore the content
        - Artisic exploration is the vangard of true innovation
**References:**
1. Antonin Artaud, The Theater and Its Double
2. Susan K. Langer, Feeling and Form
3. David Kursh, Inteligent Design of Space
4. Lanier, J. (1999). Virtual Reality. _Whole Earth Catalog_ 
5. Orwell, 1984
6. Marshall McLuhan

---

## The Script

### Separation of concepts from modality

The first used of the term “virtual reality,” as far as my research has shown, was by Antonin Artaud in his 1938 book, “The Theater and its Double.” In it he spoke of the need to create a language “intent for the senses and independent of speech” and that this language would be able to expresses thoughts and feelings “beyond the reach of the spoken language.”

The reason I like bringing this example up is because it’s disarming. At once it ties what we might commonly assume to be a modern-day development, too a much deeper lineage of thought. In forums like we find ourselves in currently, where we are discussing the future orientation of emerging technologies, the excitement for the new technology often sidelines the metaphysical questions, that I feel, could lead to a deeper understanding of essence, and what these technologies could represent. 

For example, virtual reality's domain of possibilities for is often limited to what can be achieved using a headset. But does virtual reality begin and end with a headset? Or is contemporary VR technology just a very effective means of achieving this effect? What actually distinguishes VR from your typical desktop computer experience? Similarly, what is at the core of blockchain? 

In the spirt of these questions, my intention during this presentation is to propose that the essence of virtual reality is space, that the essence of blockchain is time (and a mechanism for recording events over time), and that any sort of future metaverse would require these two ideas working in concert.

### Space is the essential character of VR

So what is the essence of virtual reality? I propose that it is space, and modern spatial computing technologies enable us to occupy, and manipulate, virtual space like never before. I’m not sure that this idea of space goes unnoticed, but my sense is that space gets taken for granted. 

In his article “The Intelligent Use of Space,” David Kirsh wrote about how space is likewise ignored in studies of human action. Pointing out that we are spatially located creatures, he says that, “How we manage the space around us…is not an afterthought; it is an integral part of the way we think, plan and behave, a central element in the way we shape the very world that constrains and guides our behavior.” Indeed, we shape space and, it shapes us.

To give a specific example of how we use space, consider the directions on the box of lasagna I made last night.

According to Kirsh, space is a resource that used to construct spatial hierarchies in order to reduce our cognitive load when achieving solving complex problems. But space is also integral to how we simply experience the world around us. Susanne K. Langer, in her book "Feeling and Form," said “space is a substrate of all our experience, gradually discovered by the collaboration of our several senses.” Often time experience is the word we use when we speak of the benefits of virtual reality. But without space we have no chance of experience.

So when we question how the development of virtual reality will play out, consider that space will be central to this development. We can spatially interact with data and leverage skills that come natural to us by nature of use being spatial beings. What if we could define our problems spatially. What if we could preform complex problems by defining them spatially? What shape are my taxes and what designed space will help me file them, for instance.

Certainly, in a future metaverse, space will likewise be the substrate of all that is. We will consume it, exchange it, and form it.

So that’s enough about space, more can be said, but I want to move on to time.

### Time is essential to blockchain

Similar to how space goes unnoticed in virtual reality, time goes unnoticed in the general discussion of blockchain, but I believe this is a mistake. Consider that time and its accurate accounting is the essence of blockchain. Time is necessary for us a temporally located creatures, to echo Kirsh. We have are always facing a temporal direction and we use that as a compass to make sense of events, past and future.

Time is integral to our formation of identity. In the movie Memento, the main character lacks a short-term memory and is forced to uses photos and tattoos in order to help him tie his experience together. Time is essential for how we develop our experience, just as space is essential.

Time is also necessary for the development of culture. The recent episode (ep. 98) of the Lore podcast, was timely in that it had a quote “history is a soil that has everything a universe needs to grow. Culture and events act like nutrients, helping a place move forward to thrive and spread.” As individuals we need to look back and see a history, as well as do communities. This becomes a tough question when we have to consider how to achieve this in the metaverse.

### Reality exists when trust exists

Consider how Jaron Lanier defined virtual reality when he coined the term in the early 80’s. “A ‘reality’ results when a mind has faith that other minds share enough of the same world to establish communication and empathy.” I think the key point in Lanier’s definition is the idea that a reality develops around a focal point of trust. If we all have an agreed upon history of events, then we inhabit the same reality. 

Consider a stack of logs, each log has its own history as indicated by its rings. Looking at this stack of logs, you could say that there is no, to little, consensus of historic events, because each log has a different set of rings. If reality is contingent on consensus, there is no reality here. Now consider how history is recorded by geologic forces for a large geographical region. These rock strata represent a solid record of history that can stand as a reference for all. We can make different moral statements about the meaning of the historic record, but we can't deny the events.

The point to be made is that a metaverse is a virtual reality that that will enable communication and interaction between its constituents. Though, for this to be a reality that we all agree to, we must trust that it historic events happened as they appear. Transactions, contracts, and such, will all require a democratic consensus that blockchain can provide.

### Conclusion

Marshall McLuhan said that when a new technological environment is made available to us, we confuse the new environment as an extension of its antecedent. Our minds are unable to think outside the proverbial box. He sees the story of Narcisist as an allegory for how we become infatuated with technology, just as Narcisist was lured in by his image, unable to see it as an image. Technology lure us in, such that we become numb to possibilities beyond the current environment. In fact, the name Narcisist is derived from a Greek word meaning numbness.

When the moving pictures first came out, they simply stuck the camera in the back of a theater and recorded the performance as if you were seated in the back of the house. There was no concept of the moving picture as separate from the the theater. It wasn’t until Metropolis and Citizen Kane that we got a glimpse of what, by essence, film was.

I believe this is why we see things like internet browsers in space and Netflix in VR, as well as why blockchain is being seen as a blanket replacement for traditional databases. This is our natural tendency to impart our current experience in a new technological environment.

So what is the way forward, how do we brake the spell of our current technological environment? McLuhan that this is inherently an artistic endeavor. That artists are able to “snap us out of the Narcissus-narcosis.” This is especially true when two new environments meet, e.g. virtual reality and blockchain. I would add that it’s not just the combination of two technologies, but the combining of their essential characters that will propel us forward into a new frontier.

Thus has been my intention during this presentation, to propose that the essential characteristics virtual reality is space and of blockchain, time. Further more, that these two technologies are essential to creating a shared and trusted metaverse. These proposals should be debated, but ultimately I feel it's important that we be considering the metaphysical characteristics of these new media. This does not invalidate all other discussion happening around the technology, but seeks to listen for the message rather than the content, as McLuhan would suggest.

Getting back to our friend Antonin, who was concerned with a narcissistic theater of his time, said, “for me the theater is identical with its possibilities for realization when the most extreme poetic results are derived from them.”

Be extreme poets!