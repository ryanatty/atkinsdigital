---
layout: page
title: Reading List
permalink: /readinglist/
---

The following resources have been of great inspiration for me as I continue my study of technology and culture. I encourage you to explore each of them in your own way. There are a range of topics from technology to philosophy, art and science, old ideas and new ones. Every one of the authors is insightful and inspiring. I'll continue to add to this list as I come across new resources.

I’ve included links to amazon.com, though please consider first borrowing from your local library or buying from a local used or new book store. I prefer buying used books for a number of reasons, which you can do on amazon.com and it will save you a lot of money.

<span style="background:yellow">**TOP TIP:**</span> If you’re into audio books, like I am, checkout the app [Libby](https://libbyapp.com/) which allows you to borrow audio books and other e-books from your library (i.e. for free!) and listen/read them on your phone or tablet. The app is really well done, and is as good, if not better, than the kindle and audible apps! Note that your library system is signed up with Libby for you to gain access.

Other great resources for books that are in the public domain are [Project Gutenberg](http://www.gutenberg.org/wiki/Main_Page) and [LibriVox](https://librivox.org/).

If you want to keep up-to-date with this list as I add new resources, or have some resources you would like to recommend, please drop me an email at **ryan at atkins dot digital**.

Happy exploring!
---

<br>
<ul class="resource-list">
  {% for resource in site.data.resources %}
    {% include resource-summary.html resource=resource %}
  {% endfor %}
</ul>
